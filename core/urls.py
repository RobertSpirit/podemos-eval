# -*- encoding: utf-8 -*-
"""
MIT License
Copyright (c) 2019 - present AppSeed.us
"""

from django.contrib import admin
from django.urls import path, include  # add this
from django.conf import settings 
from django.conf.urls.static import static 


urlpatterns = [
    path('admin/', admin.site.urls),
    path("auth/", include("authentication.urls")),  # add this
    path("dashboard/", include("dashboard.urls")),  # add this
    path("", include("page.urls"))  # add this
]
if settings.DEBUG: 
        urlpatterns += static(settings.MEDIA_URL, 
                            document_root=settings.MEDIA_ROOT) 
