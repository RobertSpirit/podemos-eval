from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, get_object_or_404, redirect
from django.template import loader
from django.http import HttpResponse
from django import template
from dashboard.forms import contactoForm
# Create your views here.
def index(request):
    return render(request, "page/index.html", {"data": {}})

def solicitud(request):
    msg = None
    form = contactoForm(request.POST) 
    if request.method == 'POST':
        if form.is_valid():
            titulo = form.cleaned_data.get("Titulo") 
            descripcion = form.cleaned_data.get("Descripcion") 
            estados = form.cleaned_data.get("Estado") 
            ciudad = form.cleaned_data.get("Ciudad") 
            obj = Reportes.objects.create(
                titulo = titulo,
                descripcion = descripcion,
                estado = estados,
                ciudad = ciudad
            )
            obj.save() 
        else:    
            msg = 'Es necesario ingresar los campos' 
    return render(request, "page/solicitar-credito.html", {"form": contactoForm, "msg" : msg, "publicidad": {}})