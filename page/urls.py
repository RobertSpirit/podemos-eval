# -*- encoding: utf-8 -*-
"""
MIT License
Copyright (c) 2019 - present AppSeed.us
"""

from django.urls import path, re_path
from page import views

urlpatterns = [
    path('', views.index, name='home'),
    path('solicitar/', views.solicitud, name='solicitud')
]
