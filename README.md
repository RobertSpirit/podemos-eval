# [Django - podemos-eval]

> **Proyecto generado** con el framework **Django Framework**

> Nota:Para realizar la configuración a la base de datos modificar el archivo .env encontrado en la raíz del proyecto.
>
> Se encuentra una carpeta con el nombre datos-prueba.sqL para cargar los datos de prueba o se pueden generar.
> En caso que se cargen los datos anexo los usuarios existentes
>
> Usuario Administrador : 
>  User: admin
>  Password: admin
>
>Cliente: 
>  User: usuario1
>  Password : password
>
> Se modificaron los horarios que existian en el sql enviado, ya que se genero un calendario donde se muestran los datos, por fines
> de prueba se rehicieron los datos

## Instalación

```bash
$ # Obtener el codigo
$ git clone https://gitlab.com/RobertSpirit/podemos-eval.git
$ cd podemos-eval
$
$ # Instalación de un entorno virtual (basado en un servidor Unix)
$ virtualenv env
$ source env/bin/activate
$
$ # Instalación de un entorno virtual (basado en un sistema de Windows)
$ # virtualenv env
$ # .\env\Scripts\activate
$
$ # Instalación de modulos
$ pip3 install -r requirements.txt
$
$ # Creanción de tablas
$ python manage.py makemigrations
$ python manage.py migrate
$
$ # Iniciar la aplicacion 
$ python manage.py runserver # por default corre en el puerto 8000
$
$ # Iniciar la aplicación en un puerto especifico
$ # python manage.py runserver 0.0.0.0:<el puerto>
$
$ # Accede pro medio de tu navegador a : http://127.0.0.1:8000/
```


## TEST

```bash
$ # Obtener el codigo
$ git clone https://gitlab.com/RobertSpirit/podemos-eval.git
$ cd podemos-eval
$
$ # Instalación de un entorno virtual (basado en un servidor Unix)
$ virtualenv env
$ source env/bin/activate
$
$ # Instalación de un entorno virtual (basado en un sistema de Windows)
$ # virtualenv env
$ # .\env\Scripts\activate
$
$ # Instalación de modulos
$ pip3 install -r requirements.txt
$
$ # Creanción de tablas
$ python manage.py makemigrations
$ python manage.py migrate
$
$ # Iniciar la aplicacion 
$ python manage.py runserver # por default corre en el puerto 8000
$
$ # Iniciar test 
$ python manage.py test # por default corre en el puerto 8000
$
$ # Iniciar la aplicación en un puerto especifico
$ # python manage.py runserver 0.0.0.0:<el puerto>
$
$ # Accede pro medio de tu navegador a : http://127.0.0.1:8000/
```

> Nota: La aplicación necesita 2 usuarios creados por medio del comando **createsuperuser**.
>
> El usuario **administrador** puede crear usuarios, grupos, miembros y asignar prestamos, para generar este usuario al registrarlo debe de contener el correo **admin@admin.com**.
>
> El usuario **cliente(s)** puede ver el calendario de sus pagos y realizar el pago, una vez que se le haya asignado un prestamo (Esta seccion se encuentra dentro del dashboard del usuario admin, creado con anterioridad), se necesita generar un acceso para los usuarios del grupo <Un acceso por cuenta>
>


<br />

## Estructura del codgo
El proyecto contiene una estructura simple e intuitiva que se presenta a continuación:

```bash
< PROJECT ROOT >
   |
   |-- core/                             
   |    |-- settings.py                   
   |    |-- wsgi.py                       
   |    |-- urls.py                        
   |    |
   |    |-- static/
   |    |    |-- <css, JS, images>         
   |    |
   |    |-- templates/                    
   |         |
   |         |-- includes/               
   |         |    |-- navigation.html      
   |         |    |-- sidebar.html             
   |         |    |-- scripts.html         
   |         |
   |         |-- layouts/                  
   |         |    |-- base-fullscreen.html 
   |         |    |-- base.html  
   |         |    |-- baserep.html           
   |         |
   |         |-- accounts/                 
   |         |    |-- login.html
   |         |
   |         |-- dashboard/                 
   |         |    |-- *.html
   |         |
   |         |-- page/                 
   |         |    |-- *.html              
   |         |          
   |         |                        
   |
   |-- authentication/                     
   |    |
   |    |-- urls.py                                     
   |    |-- views.py                         
   |    |-- forms.py                       
   |
   |-- dashboard/                               
   |    |
   |    |-- views.py                       
   |    |-- urls.py                         
   |
   |-- page/                                
   |    |
   |    |-- views.py                      
   |    |-- urls.py 
   |
   |-- db/
   |    |-- Modelo D-E-R.png
   |    |-- datos-prueba.sql                 
   |
   |-- requirements.txt                    
   |
   |-- .env                               
   |-- manage.py                           
   |
   |-- ************************************************************************
```

<br />

> Flujo de arranque
- Django bootstrapper manage.py usa core / settings.py como archivo de configuración principal
- core / settings.py carga la magia de la aplicación desde el archivo .env
- Redirigir a los usuarios que gusten acceder al dashboard a la página de inicio de sesión si no cuentan con una sesion generada

<br />

## Modificaciones 

- Se agrego una relación en la tabla clientes hacía la tabla auth que viene por default en DJANGO
para poder generar una cuenta a un grupo de clientes y asi tengas acceso al sistema <Se anexa el D-E-R en la carpeta DB junsto con scripts de prueba >

<br />

## Creditos 

- [Django](https://www.djangoproject.com/) - Pagina Oficial
- [Roberto Espiritu Ramirez](https://www.linkedin.com/in/spiritrobert) - Desarrollador del codigo


<br />

## API 

- Parametro : ID del grupo
   Respuesta:
      Todos los usuarios de ese grupo (JSON)

   Example:
   http://localhost:8000/dashboard/clientesgrupos/IDGrupo


<br />

