-- MySQL dump 10.13  Distrib 8.0.22, for Win64 (x86_64)
--
-- Host: localhost    Database: podemos_eval
-- ------------------------------------------------------
-- Server version	8.0.22

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auth_group` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auth_group_permissions` (
  `id` int NOT NULL AUTO_INCREMENT,
  `group_id` int NOT NULL,
  `permission_id` int NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auth_permission` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content_type_id` int NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`),
  CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add log entry',1,'add_logentry'),(2,'Can change log entry',1,'change_logentry'),(3,'Can delete log entry',1,'delete_logentry'),(4,'Can view log entry',1,'view_logentry'),(5,'Can add permission',2,'add_permission'),(6,'Can change permission',2,'change_permission'),(7,'Can delete permission',2,'delete_permission'),(8,'Can view permission',2,'view_permission'),(9,'Can add group',3,'add_group'),(10,'Can change group',3,'change_group'),(11,'Can delete group',3,'delete_group'),(12,'Can view group',3,'view_group'),(13,'Can add user',4,'add_user'),(14,'Can change user',4,'change_user'),(15,'Can delete user',4,'delete_user'),(16,'Can view user',4,'view_user'),(17,'Can add content type',5,'add_contenttype'),(18,'Can change content type',5,'change_contenttype'),(19,'Can delete content type',5,'delete_contenttype'),(20,'Can view content type',5,'view_contenttype'),(21,'Can add session',6,'add_session'),(22,'Can change session',6,'change_session'),(23,'Can delete session',6,'delete_session'),(24,'Can view session',6,'view_session'),(25,'Can add calendario pagos',7,'add_calendariopagos'),(26,'Can change calendario pagos',7,'change_calendariopagos'),(27,'Can delete calendario pagos',7,'delete_calendariopagos'),(28,'Can view calendario pagos',7,'view_calendariopagos'),(29,'Can add clientes',8,'add_clientes'),(30,'Can change clientes',8,'change_clientes'),(31,'Can delete clientes',8,'delete_clientes'),(32,'Can view clientes',8,'view_clientes'),(33,'Can add cuentas',9,'add_cuentas'),(34,'Can change cuentas',9,'change_cuentas'),(35,'Can delete cuentas',9,'delete_cuentas'),(36,'Can view cuentas',9,'view_cuentas'),(37,'Can add grupos',10,'add_grupos'),(38,'Can change grupos',10,'change_grupos'),(39,'Can delete grupos',10,'delete_grupos'),(40,'Can view grupos',10,'view_grupos'),(41,'Can add miembros',11,'add_miembros'),(42,'Can change miembros',11,'change_miembros'),(43,'Can delete miembros',11,'delete_miembros'),(44,'Can view miembros',11,'view_miembros'),(45,'Can add transacciones',12,'add_transacciones'),(46,'Can change transacciones',12,'change_transacciones'),(47,'Can delete transacciones',12,'delete_transacciones'),(48,'Can view transacciones',12,'view_transacciones');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auth_user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user`
--

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
INSERT INTO `auth_user` VALUES (1,'pbkdf2_sha256$120000$jd2pEUWwDwLc$xyenkPlv5u+uDmobSptR6o1SkxwcURsArpOx9q1Dc/U=','2020-12-22 04:01:50.719389',1,'admin','','','admin@admin.com',1,1,'2020-12-22 04:01:45.096124'),(2,'pbkdf2_sha256$120000$2qBEtYzRZdHw$T9lIRYUq4vYA0WPLR+hzVV+dqWaMvqcDpIdgZZ6XRL0=',NULL,1,'usuario1','','','usuario1@usuario1.com',1,1,'2020-12-22 04:16:54.002304');
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auth_user_groups` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `group_id` int NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id`,`group_id`),
  KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`),
  CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_groups`
--

LOCK TABLES `auth_user_groups` WRITE;
/*!40000 ALTER TABLE `auth_user_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auth_user_user_permissions` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `permission_id` int NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id`,`permission_id`),
  KEY `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_user_permissions`
--

LOCK TABLES `auth_user_user_permissions` WRITE;
/*!40000 ALTER TABLE `auth_user_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dashboard_calendariopagos`
--

DROP TABLE IF EXISTS `dashboard_calendariopagos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `dashboard_calendariopagos` (
  `calendario_id` int NOT NULL AUTO_INCREMENT,
  `numero_pago` int NOT NULL,
  `monto` int NOT NULL,
  `fecha_pago` date NOT NULL,
  `estatus` varchar(250) NOT NULL,
  `cuentas_id` varchar(250) NOT NULL,
  PRIMARY KEY (`calendario_id`),
  KEY `dashboard_calendario_cuentas_id_3ba67b3e_fk_dashboard` (`cuentas_id`),
  CONSTRAINT `dashboard_calendario_cuentas_id_3ba67b3e_fk_dashboard` FOREIGN KEY (`cuentas_id`) REFERENCES `dashboard_cuentas` (`cuentas_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dashboard_calendariopagos`
--

LOCK TABLES `dashboard_calendariopagos` WRITE;
/*!40000 ALTER TABLE `dashboard_calendariopagos` DISABLE KEYS */;
INSERT INTO `dashboard_calendariopagos` VALUES (1,1,19999,'2020-12-28','PENDIENTE','57318'),(2,2,19999,'2021-01-04','PENDIENTE','57318'),(3,3,19999,'2021-01-11','PENDIENTE','57318'),(4,4,19999,'2021-01-18','PENDIENTE','57318');
/*!40000 ALTER TABLE `dashboard_calendariopagos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dashboard_clientes`
--

DROP TABLE IF EXISTS `dashboard_clientes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `dashboard_clientes` (
  `cliente_id` varchar(250) NOT NULL,
  `nombre` varchar(250) NOT NULL,
  PRIMARY KEY (`cliente_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dashboard_clientes`
--

LOCK TABLES `dashboard_clientes` WRITE;
/*!40000 ALTER TABLE `dashboard_clientes` DISABLE KEYS */;
INSERT INTO `dashboard_clientes` VALUES ('LIREXPT','usuario3'),('P44546V','usuario1'),('P6PKH83','usuario2');
/*!40000 ALTER TABLE `dashboard_clientes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dashboard_cuentas`
--

DROP TABLE IF EXISTS `dashboard_cuentas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `dashboard_cuentas` (
  `cuentas_id` varchar(250) NOT NULL,
  `estatus` varchar(250) NOT NULL,
  `monto` int NOT NULL,
  `saldo` int NOT NULL,
  `grupos_id` varchar(250) NOT NULL,
  `login_id` int NOT NULL,
  PRIMARY KEY (`cuentas_id`),
  KEY `dashboard_cuentas_grupos_id_480a0cef_fk_dashboard` (`grupos_id`),
  KEY `dashboard_cuentas_login_id_9576a5aa_fk_auth_user_id` (`login_id`),
  CONSTRAINT `dashboard_cuentas_grupos_id_480a0cef_fk_dashboard` FOREIGN KEY (`grupos_id`) REFERENCES `dashboard_grupos` (`grupos_id`),
  CONSTRAINT `dashboard_cuentas_login_id_9576a5aa_fk_auth_user_id` FOREIGN KEY (`login_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dashboard_cuentas`
--

LOCK TABLES `dashboard_cuentas` WRITE;
/*!40000 ALTER TABLE `dashboard_cuentas` DISABLE KEYS */;
INSERT INTO `dashboard_cuentas` VALUES ('57318','DESEMBOLSADA',79998,0,'XHB3A',2);
/*!40000 ALTER TABLE `dashboard_cuentas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dashboard_grupos`
--

DROP TABLE IF EXISTS `dashboard_grupos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `dashboard_grupos` (
  `grupos_id` varchar(250) NOT NULL,
  `nombre` varchar(250) NOT NULL,
  PRIMARY KEY (`grupos_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dashboard_grupos`
--

LOCK TABLES `dashboard_grupos` WRITE;
/*!40000 ALTER TABLE `dashboard_grupos` DISABLE KEYS */;
INSERT INTO `dashboard_grupos` VALUES ('XHB3A','test');
/*!40000 ALTER TABLE `dashboard_grupos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dashboard_miembros`
--

DROP TABLE IF EXISTS `dashboard_miembros`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `dashboard_miembros` (
  `id` int NOT NULL AUTO_INCREMENT,
  `clientes_id` varchar(250) NOT NULL,
  `grupos_id` varchar(250) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `dashboard_miembros_clientes_id_92a7f404_fk_dashboard` (`clientes_id`),
  KEY `dashboard_miembros_grupos_id_ca02dcab_fk_dashboard` (`grupos_id`),
  CONSTRAINT `dashboard_miembros_clientes_id_92a7f404_fk_dashboard` FOREIGN KEY (`clientes_id`) REFERENCES `dashboard_clientes` (`cliente_id`),
  CONSTRAINT `dashboard_miembros_grupos_id_ca02dcab_fk_dashboard` FOREIGN KEY (`grupos_id`) REFERENCES `dashboard_grupos` (`grupos_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dashboard_miembros`
--

LOCK TABLES `dashboard_miembros` WRITE;
/*!40000 ALTER TABLE `dashboard_miembros` DISABLE KEYS */;
INSERT INTO `dashboard_miembros` VALUES (1,'LIREXPT','XHB3A'),(2,'P6PKH83','XHB3A');
/*!40000 ALTER TABLE `dashboard_miembros` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dashboard_transacciones`
--

DROP TABLE IF EXISTS `dashboard_transacciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `dashboard_transacciones` (
  `transacciones_id` int NOT NULL AUTO_INCREMENT,
  `fecha` date NOT NULL,
  `monto` int NOT NULL,
  `cuentas_id` varchar(250) NOT NULL,
  PRIMARY KEY (`transacciones_id`),
  KEY `dashboard_transaccio_cuentas_id_ed87f802_fk_dashboard` (`cuentas_id`),
  CONSTRAINT `dashboard_transaccio_cuentas_id_ed87f802_fk_dashboard` FOREIGN KEY (`cuentas_id`) REFERENCES `dashboard_cuentas` (`cuentas_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dashboard_transacciones`
--

LOCK TABLES `dashboard_transacciones` WRITE;
/*!40000 ALTER TABLE `dashboard_transacciones` DISABLE KEYS */;
/*!40000 ALTER TABLE `dashboard_transacciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `django_admin_log` (
  `id` int NOT NULL AUTO_INCREMENT,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int DEFAULT NULL,
  `user_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`),
  CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `django_content_type` (
  `id` int NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (1,'admin','logentry'),(3,'auth','group'),(2,'auth','permission'),(4,'auth','user'),(5,'contenttypes','contenttype'),(7,'dashboard','calendariopagos'),(8,'dashboard','clientes'),(9,'dashboard','cuentas'),(10,'dashboard','grupos'),(11,'dashboard','miembros'),(12,'dashboard','transacciones'),(6,'sessions','session');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_migrations`
--

DROP TABLE IF EXISTS `django_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `django_migrations` (
  `id` int NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_migrations`
--

LOCK TABLES `django_migrations` WRITE;
/*!40000 ALTER TABLE `django_migrations` DISABLE KEYS */;
INSERT INTO `django_migrations` VALUES (1,'contenttypes','0001_initial','2020-12-22 03:54:12.973848'),(2,'auth','0001_initial','2020-12-22 03:54:14.229704'),(3,'admin','0001_initial','2020-12-22 03:54:14.483538'),(4,'admin','0002_logentry_remove_auto_add','2020-12-22 03:54:14.497657'),(5,'admin','0003_logentry_add_action_flag_choices','2020-12-22 03:54:14.511584'),(6,'contenttypes','0002_remove_content_type_name','2020-12-22 03:54:14.683246'),(7,'auth','0002_alter_permission_name_max_length','2020-12-22 03:54:14.785414'),(8,'auth','0003_alter_user_email_max_length','2020-12-22 03:54:14.817787'),(9,'auth','0004_alter_user_username_opts','2020-12-22 03:54:14.828724'),(10,'auth','0005_alter_user_last_login_null','2020-12-22 03:54:14.921550'),(11,'auth','0006_require_contenttypes_0002','2020-12-22 03:54:14.926863'),(12,'auth','0007_alter_validators_add_error_messages','2020-12-22 03:54:14.942854'),(13,'auth','0008_alter_user_username_max_length','2020-12-22 03:54:15.127828'),(14,'auth','0009_alter_user_last_name_max_length','2020-12-22 03:54:15.431019'),(15,'dashboard','0001_initial','2020-12-22 03:54:16.748839'),(16,'dashboard','0002_auto_20201220_2216','2020-12-22 03:54:16.841916'),(17,'sessions','0001_initial','2020-12-22 03:54:16.902919');
/*!40000 ALTER TABLE `django_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_expire_date_a5c62663` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'podemos_eval'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-12-21 22:19:44
