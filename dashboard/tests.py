from django.test import TestCase
from .models import Clientes, Grupos, Miembros, Cuentas, Transacciones, CalendarioPagos
from django.contrib.auth.models import User
import unittest
from django.test import Client


class DatabaseTestCase(TestCase):
    def setUp(self):
        user = User.objects.create_superuser('test', 'test@test.com', 'test')
        cliente = Clientes.objects.create(nombre="test")
        grupo = Grupos.objects.create(nombre="test")
        miembros = Miembros.objects.create(grupos=grupo, clientes=cliente)
        cuentas = Cuentas.objects.create(cuentas_id='SDASD2',
                                        grupos = grupo ,
                                        estatus = 'PENDIENTE',
                                        monto = 6000 ,
                                        saldo = 0,
                                        login = user)
        transaccion = Transacciones.objects.create(cuentas = cuentas,
                                                    fecha = '2020-12-30',
                                                    monto = '200000')
        calentario = CalendarioPagos.objects.create(cuentas = cuentas,
                                                    numero_pago = 4,
                                                    monto = 200000,
                                                    fecha_pago = '2020-12-31',
                                                    estatus = 'PENDIENTE')
    
    def test_clientes(self):
        cliente = Clientes.objects.get(nombre="test")

    def test_grupos(self):
        grupo = Grupos.objects.get(nombre="test")

    def test_miembros(self):
        miembros = Miembros.objects.get(pk=5)

        
    def test_cuentas(self):
        cuentas = Cuentas.objects.get(cuentas_id='SDASD2')

    def test_transacciones(self):
        transaccion = Transacciones.objects.get(monto=200000)

    def test_calendario(self):
        calendario = CalendarioPagos.objects.get(pk=1)


class UrlTest(unittest.TestCase):
    def setUp(self):
        # Every test needs a client.
        self.client = Client()

    def test_clientes(self):
        response = self.client.get('/dashboard/clientes/', follow=True)
        self.assertEqual(response.status_code, 200)
    
    def test_grupos(self):
        response = self.client.get('/dashboard/grupos/',follow=True)
        self.assertEqual(response.status_code, 200)
    
    def test_grupos(self):
        response = self.client.get('/dashboard/grupos/17EQZ',follow=True)
        self.assertEqual(response.status_code, 200)
    
    def test_prestamos(self):
        response = self.client.get('/dashboard/prestamos/',follow=True)
        self.assertEqual(response.status_code, 200)
    
    def test_clientesgrupos(self):
        response = self.client.get('/dashboard/clientesgrupos/17EQZ',follow=True)
        self.assertEqual(response.status_code, 200)
    
    def test_pago(self):
        response = self.client.get('/dashboard/pago/',follow=True)
        self.assertEqual(response.status_code, 200)
    
    def test_transaccion(self):
        response = self.client.get('/dashboard/transaccion/',follow=True)
        self.assertEqual(response.status_code, 200)
