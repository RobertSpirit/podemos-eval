# -*- encoding: utf-8 -*-
"""
MIT License
Copyright (c) 2019 - present AppSeed.us
"""

from django.urls import path, re_path
from dashboard import views

urlpatterns = [
    path('', views.index, name='home'),
    path('clientes/', views.clientes, name='clientes'),
    path('grupos/', views.grupos, name='grupos'),
    path('grupos/<str:queryparams>', views.gruposUsuarios, name='gruposids'),
    path('prestamos/', views.prestamos, name='prestamos'),
    path('clientesgrupos/<str:grupos>', views.get_users, name='obtenerclientesporgrupos'),
    path('calendar/<str:cuenta>', views.calendario, name='calendario'),
    path('calendario-usuarios/', views.calendarioUsuarios, name='calendarioUsuario'),
    path('deleteUser/<str:identificador>', views.delete_user, name='eliminarusuario'),
    path('pago/', views.pago, name='pago'),
    path('transaccion/', views.transaccion, name='transaccion')
]
