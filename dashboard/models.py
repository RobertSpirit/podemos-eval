# -*- encoding: utf-8 -*-
"""
MIT License
Copyright (c) 2019 - present AppSeed.us
"""

from django.db import models
from django.contrib.auth.models import User

class Clientes(models.Model):
    cliente_id = models.CharField(primary_key=True, max_length=250) 
    nombre = models.CharField(max_length=250)

    def __str__(self):
        return "%s %s" % (
            self.cliente_id,
            self.nombre)

class Grupos(models.Model):
    grupos_id = models.CharField(primary_key=True, max_length=250)
    nombre = models.CharField(max_length=250)

    def __str__(self):
        return "%s %s" % (
            self.grupos_id,
            self.nombre)

class Miembros(models.Model):
    grupos = models.ForeignKey(Grupos, on_delete=models.CASCADE)
    clientes = models.ForeignKey(Clientes, on_delete=models.CASCADE)

    def __str__(self):
        return "%s %s" % (
            self.grupos,
            self.clientes)

class Cuentas(models.Model):
    cuentas_id = models.CharField(primary_key=True, max_length=250)
    grupos = models.ForeignKey(Grupos, on_delete=models.CASCADE)
    estatus = models.CharField(max_length=250)
    monto = models.IntegerField()
    saldo = models.IntegerField()
    login = models.ForeignKey(User, on_delete=models.CASCADE)
    
    def __str__(self):
        return "%s %s %s %s %s %s" % (
            self.cuentas_id,
            self.grupos,
            self.estatus, 
            self.monto,
            self.saldo,
            self.login)

class Transacciones(models.Model):
    transacciones_id = models.AutoField(primary_key=True)
    cuentas = models.ForeignKey(Cuentas, on_delete=models.CASCADE)
    fecha = models.DateField(auto_now = False , auto_now_add = False)
    monto = models.IntegerField()
    def __str__(self):
        return "%s %s %s %s" % (
            self.transacciones_id,
            self.cuentas,
            self.fecha, 
            self.monto)

class CalendarioPagos(models.Model):
    calendario_id = models.AutoField(primary_key=True)
    cuentas = models.ForeignKey(Cuentas, on_delete=models.CASCADE)
    numero_pago = models.IntegerField()
    monto = models.IntegerField()
    fecha_pago = models.DateField(auto_now = False , auto_now_add = False)
    estatus = models.CharField(max_length=250)
    
    def __str__(self):
        return "%s %s %s %s %s %s" % (
            self.calendario_id,
            self.cuentas,
            self.numero_pago, 
            self.monto,
            self.fecha_pago,
            self.estatus)

