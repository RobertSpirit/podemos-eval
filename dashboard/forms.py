# -*- encoding: utf-8 -*-
"""
MIT License
Copyright (c) 2019 - present AppSeed.us
"""

from django import forms
from .models import Grupos, Cuentas

class contactoForm(forms.Form):
    nombre = forms.CharField(
        widget=forms.TextInput(
            attrs={
                "placeholder" : "Nombre",                
                "class": "form-control"
            }
        ))
    correo = forms.CharField(
        widget=forms.TextInput(
            attrs={
                "placeholder" : "Correo Electrónico",                
                "class": "form-control",
            }
        ))
    numero = forms.CharField(
        widget=forms.TextInput(
            attrs={
                "placeholder" : "Número principal de contacto",                
                "class": "form-control",
            }
        ))
    contacto = forms.CharField(
        required=False,
        widget=forms.TextInput(
            attrs={
                "placeholder" : "Otro número de contacto que quieras dejarnos",                
                "class": "form-control",
            }
        ))
    grupo= forms.CharField(
        required=False,
        widget=forms.CheckboxInput(
            attrs={        
                "class": "form-check-input",
            }
        ))
    credito= forms.CharField(
        required=False,
        widget=forms.CheckboxInput(
            attrs={            
                "class": "form-check-input",
            }
        ))
    comentarios= forms.CharField(
        widget=forms.Textarea(
            attrs={
                "placeholder" : "Queremos saber más de ti. ¿en qué podemos ayudarte?",                
                "class": "form-control",
                "rows": 3
            }
        ))

class clienteForm(forms.Form):
    nombre = forms.CharField(
        widget=forms.TextInput(
            attrs={
                "placeholder" : "Nombre del usuario",                
                "class": "form-control"
            }
        ))

class grupoForm(forms.Form):
    nombre = forms.CharField(
        widget=forms.TextInput(
            attrs={
                "placeholder" : "Nombre del grupo",                
                "class": "form-control"
            }
        ))
    
class prestamoForm(forms.Form):
    cantidad = forms.IntegerField(
        widget=forms.NumberInput(
            attrs={
                "placeholder" : "Cantidad del prestamo",                
                "class": "form-control"
            }
        ), required=False)
    usuario = forms.CharField(
        widget=forms.TextInput(
            attrs={
                "placeholder" : "Nombre de usuario",                
                "class": "form-control"
            }
        ))
    password = forms.CharField(
        widget=forms.TextInput(
            attrs={
                "placeholder" : "password",                
                "class": "form-control",
                "type": "password"
            }
        ))
    email = forms.CharField(
        widget=forms.TextInput(
            attrs={
                "placeholder" : "Correo electronico",                
                "class": "form-control"
            }
        ))
    array = [] 
    try:
        for item in Grupos.objects.values():
            array.append((item['grupos_id'], item['nombre']))
    except:
        pass
    grupo = forms.ChoiceField(
        widget=forms.Select(
            attrs={             
                "class": "form-control",
                "id": "select"
            }
        ), choices=array)

class calendarioForm(forms.Form):
    array = [] 
    try:
        for item in Cuentas.objects.values():
            array.append((item['cuentas_id'], item['grupos_id']))
    except:
        pass
    cuenta = forms.ChoiceField(
        widget=forms.Select(
            attrs={             
                "class": "form-control",
                "id": "select"
            }
        ), choices=array)