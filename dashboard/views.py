# -*- encoding: utf-8 -*-
"""
Copyright (c) 2019 - present AppSeed.us
"""

from django.contrib.auth.decorators import login_required
from django.shortcuts import render, get_object_or_404, redirect
from django.template import loader
from django.http import HttpResponse
from django import template
from django.core import serializers
import random
import string
import json
from django.core.serializers.json import DjangoJSONEncoder
from django.http import JsonResponse
from datetime import datetime, timedelta
#Importa los formularios o modelos
from .forms import clienteForm, grupoForm, prestamoForm, calendarioForm
from .models import  Clientes, Grupos, Miembros, Cuentas, CalendarioPagos, Transacciones
from django.contrib.auth.models import User


def get_dates(number):
        newDate = datetime.now()+timedelta(days=number)
        return newDate.strftime('%Y-%m-%d')


def get_users(request, grupos):
    response= json.dumps({"respose": False})
    try:
        array = []
        data_json = serializers.serialize('json', Miembros.objects.filter(grupos_id=grupos))
        miembros = json.loads(data_json)
        for item in miembros:
            result = Clientes.objects.filter(cliente_id=item['fields']['clientes'])
            data_json = serializers.serialize('json', result)
            clientes = json.loads(data_json)
            for cliente in clientes:
                array.append({'value':cliente['fields']['nombre']})
            response = json.dumps({"respose":array})
    except:
            response = json.dumps({"respose":False})
    return HttpResponse(response, content_type="application/json")


def generate_Id(tamaño):
    ids = ''.join([random.choice(string.ascii_uppercase + string.digits)  for n in range(tamaño)])  
    return ids  


def generate_id_number(tamaño):
    ids = ''.join([random.choice(string.digits)  for n in range(tamaño)])  
    return ids  


def delete_user(request, identificador):
    user = Clientes.objects.get(cliente_id=identificador)
    user.delete()
    return HttpResponse('OK')


@login_required(login_url="/auth/login/")
def index(request):
    clientes = Clientes.objects.count()
    grupos = Grupos.objects.count()
    cuentasconteo = Cuentas.objects.count()
    cuentas = Cuentas.objects.values_list('monto', flat=True)
    saldo = 0
    idCalendar = 1234
    for item in cuentas:
        saldo = saldo + item
    admin=None
    if request.user.email == 'admin@admin.com':
        admin='Administrador'
    else:
        ids = request.user.id
        obtenerCuenta = Cuentas.objects.filter(login_id=ids)
        data_json = serializers.serialize('json', obtenerCuenta)
        result = json.loads(data_json)
        for item in result:
            idCalendar = item['pk']
            return redirect("/dashboard/calendar/{}".format(item['pk']))
    return render(request, 'dashboard/index.html', {'admin': admin, 'clientes': clientes, 'grupos': grupos, 'cuentas':cuentasconteo, 'saldo': saldo, 'calendar': idCalendar})


@login_required(login_url="/auth/login/")
def clientes(request):
    admin=None
    msg = None
    data = Clientes.objects.all()
    if request.user.email == 'admin@admin.com':
        admin='Administrador'
    form = clienteForm(request.POST) 
    if request.method == 'POST':
        if form.is_valid():
            nombre = form.cleaned_data.get("nombre") 
            ids = generate_Id(7)
            newclient = Clientes.objects.create(
                cliente_id=ids,
                nombre=nombre
            )
            newclient.save()
    return render(request, "dashboard/clientes.html", {'admin': admin, 'form': clienteForm, 'data': data})


@login_required(login_url="/auth/login/")
def grupos(request):
    admin=None
    msg = None
    data = Grupos.objects.all()
    if request.user.email == 'admin@admin.com':
        admin='Administrador'
    form = grupoForm(request.POST) 
    if request.method == 'POST':
        if form.is_valid():
            nombre = form.cleaned_data.get("nombre") 
            ids = generate_Id(5)
            newgrupo = Grupos.objects.create(
                grupos_id=ids,
                nombre=nombre
            )
            newgrupo.save()
        else:
            grupoid = request.POST['grupo']
            usersarray = request.POST.getlist('users[]')
            miembros = Miembros.objects.filter(grupos=grupoid)
            miembros.delete()
            for item in usersarray:
                if item:
                    user = Clientes.objects.get(cliente_id=item)
                    grupo = Grupos.objects.get(grupos_id=grupoid)
                    newGrupUsers=Miembros.objects.create(
                        grupos=grupo,
                        clientes=user
                    )
                    newGrupUsers.save()
                else:
                    pass
            return HttpResponse ('OK')
    return render(request, "dashboard/grupos.html", {'admin': admin, 'form': grupoForm, 'data': data})


@login_required(login_url="/auth/login/")
def gruposUsuarios(request, queryparams):
    array = []
    data = Clientes.objects.all()
    data_json = serializers.serialize('json', Miembros.objects.filter(grupos_id=queryparams))
    miembros = json.loads(data_json)
    for item in miembros:
        array.append(item['fields']['clientes'])
    admin=None
    msg = None
    if request.user.email == 'admin@admin.com':
        admin='Administrador'
    return render(request, "dashboard/asignar.html", {'admin': admin, 'data':data, 'grupo': queryparams, 'datagroup':array})


@login_required(login_url="/auth/login/")
def prestamos(request):
    array = []
    displayPrestamo = 'none'
    admin=None
    msg = None
    if request.user.email == 'admin@admin.com':
        admin='Administrador'
    form = prestamoForm(request.POST) 
    if request.method == 'POST':
        if form.is_valid():
            grupo = form.cleaned_data.get("grupo")
            prestamo = form.cleaned_data.get("cantidad") 
            usuario = form.cleaned_data.get("usuario")
            password = form.cleaned_data.get("password")
            email = form.cleaned_data.get("email")
            newLogin = User.objects.create_superuser(usuario, email, password)
            newLogin.save()
            newCount = Cuentas.objects.create(
                cuentas_id = generate_id_number(5),
                grupos = Grupos.objects.get(grupos_id=grupo),
                estatus = 'DESEMBOLSADA',
                monto = prestamo,
                saldo = 0,
                login= User.objects.get(pk=newLogin.pk),
            )
            newCount.save()
            conteo = 7
            for i in range(4):
                date = get_dates(conteo)
                conteo = conteo+7
                newCalendar = CalendarioPagos.objects.create(
                    cuentas = Cuentas.objects.get(cuentas_id=newCount.cuentas_id),
                    numero_pago = i + 1,
                    monto = prestamo / 4,
                    fecha_pago = date,
                    estatus = 'PENDIENTE'
                )
                newCalendar.save()
            return redirect("/dashboard/calendar/{}".format(newCount.cuentas_id))
            if prestamo <= 0:
                msg = "El prestamo debe ser mayor a 0"
    return render(request, "dashboard/prestamos.html", {'admin': admin, 'form':prestamoForm, 'msg': msg, 'display':displayPrestamo})


@login_required(login_url="/auth/login/")
def calendario(request, cuenta):
    array = []
    admin=None
    msg = None
    obtenerHorarios = CalendarioPagos.objects.filter(
        cuentas = Cuentas.objects.get(cuentas_id=cuenta)
    )
    data_json = serializers.serialize('json', obtenerHorarios)
    data = json.loads(data_json)
    for item in data:
        if item['fields']['estatus'] != 'PAGADO':
            array.append(
                {
                    'title': 'Se tiene que realizar un pago de {}'.format(item['fields']['monto']),
                    'start': item['fields']['fecha_pago'],
                    'className': 'important',
                    'allDay': True
                }
            )
        else:
            array.append(
                {
                    'title': 'Se realizo el pago {}'.format(item['fields']['monto']),
                    'start': item['fields']['fecha_pago'],
                    'className': 'success',
                    'allDay': True
                }
            )
    response = json.dumps(list(array), cls=DjangoJSONEncoder)
    if request.user.email == 'admin@admin.com':
        admin='Administrador'
    return render(request, "dashboard/calendario.html", {'admin': admin, 'data': response})


@login_required(login_url="/auth/login/")
def calendarioUsuarios(request):
    admin=None
    if request.user.email == 'admin@admin.com':
        admin='Administrador'
    form = calendarioForm(request.POST) 
    if request.method == 'POST':
        if form.is_valid():
            cuentaSeleccionada = form.cleaned_data.get("cuenta")
            return redirect("/dashboard/calendar/{}".format(cuentaSeleccionada))
    return render(request, "dashboard/calendarioUsuario.html", {'admin': admin, 'form': calendarioForm})


@login_required(login_url="/auth/login/")
def pago(request):
    array = []
    admin = None
    if request.user.email == 'admin@admin.com':
        admin='Administrador'
    else:
        ids = request.user.id
        obtenerCuenta = Cuentas.objects.filter(login_id=ids)
        data_json = serializers.serialize('json', obtenerCuenta)
        result = json.loads(data_json)
        CalendarioPago = CalendarioPagos.objects.filter(cuentas_id=result[0]['pk'], )
        calendario_json = serializers.serialize('json', CalendarioPago)
        resultCalendar = json.loads(calendario_json)
        for items in resultCalendar:
            if items['fields']['estatus'] != 'PAGADO':
                array.append({'data':items['fields']['fecha_pago'], 'estatus':items['fields']['estatus'], 'id':items['pk'] })
    return render(request, "dashboard/pagos.html", {'admin': admin, 'data': array, })


@login_required(login_url="/auth/login/")
def transaccion(request):
    array = []
    admin = None
    monto = None
    cuenta = None
    saldo = None
    adeudo = None
    if request.method == 'POST':
        fecha = request.POST['data']
        identificador = request.POST['identificador']
        actializarCalendario = CalendarioPagos.objects.filter(calendario_id=identificador,fecha_pago=fecha)
        data_json = serializers.serialize('json', actializarCalendario)
        result = json.loads(data_json)
        for items in result:
            monto = items['fields']['monto']
            cuenta = items['fields']['cuentas']
        actializarCalendario.update(estatus='PAGADO')
        obtenerCuentas = Cuentas.objects.filter(cuentas_id=cuenta)
        data_json = serializers.serialize('json', obtenerCuentas)
        resultCuentas = json.loads(data_json)
        for item in resultCuentas:
            saldo = item['fields']['saldo']
            adeudo = item['fields']['monto']
        if adeudo == saldo+monto:
            ActualizarCuenta = Cuentas.objects.filter(cuentas_id=cuenta).update(saldo=saldo+monto, estatus='CERRADA')
        else:
            ActualizarCuenta = Cuentas.objects.filter(cuentas_id=cuenta).update(saldo=saldo+monto)
        newTransaccion = Transacciones.objects.create(
            cuentas= Cuentas.objects.get(cuentas_id=cuenta),
            fecha = datetime.now().strftime('%Y-%m-%d'),
            monto = monto
        )
        newTransaccion.save()
    return HttpResponse('OK')